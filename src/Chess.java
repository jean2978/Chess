import pieces.Peasant;

import javax.swing.*;
import java.awt.*;

/**
 * This class draws a chess board on a JFrame
 * @author Jesper
 * @version 1.0
 */
public class Chess {

    private static final util.ChessUtil ChessUtil = new util.ChessUtil();

    /**
     * This is the main method
     * @param args the arguments passed to the main method (not used)
     */
    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setSize(1000, 1000);
        jFrame.setTitle("Chess");
        jFrame.setResizable(false);

        drawBoard(jFrame);
        drawPieces(jFrame);
        jFrame.setLayout(null);
        jFrame.setLocationRelativeTo(null);
        jFrame.setVisible(true);
    }

    /*
     * This method draws the chess board on the JFrame
     * @param jFrame the JFrame to draw the board on
     */
    public static void drawBoard(JFrame jFrame) {
        int x = 0;
        int y = 0;
        int width = 125;
        int height = 125;

        //alternate the colors of the squares
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                JPanel jPanel = new JPanel();
                if ((i + j) % 2 == 0) {
                    jPanel.setBackground(Color.WHITE);
                } else {
                    jPanel.setBackground(Color.BLACK);
                }
                jPanel.setBounds(x, y, width, height);
                jPanel.setName(ChessUtil.getSquareName(i, j));
                jFrame.getContentPane().add(jPanel);
                x += 125;
            }
            y += 125;
            x = 0;
        }
    }

    /*
     * This method draws the pieces on the board
     * @param jFrame the JFrame to draw the pieces on
     */
    public static void drawPieces(JFrame jFrame) {
        //TODO: draw the pieces on the board
        Peasant peasant = new Peasant(0, 125, 50, 50, Color.blue);
        JPanel jpanel = (JPanel) jFrame.getContentPane().getComponent(1);
        System.out.println(jpanel.getName());
        jpanel.add(peasant);

    }
}

