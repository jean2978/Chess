package pieces;

import javax.swing.*;
import java.awt.*;

/**
 * This class represents a square on the board (a square is a JPanel)
 * @author Jesper
 * @version 1.0
 */
public class Square extends JPanel {

    private int x;
    private int y;
    private int width;
    private int height;
    private Color color;
    private boolean isOccupied;
    private String squareName;

    public Square(int x, int y, int width, int height, Color color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
