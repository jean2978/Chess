package pieces;

/**
 * A record class that represents a tower.
 *
 * @author Jesper
 * @version 1.0
 */
public record Tower(int x, int y, boolean isWhite) {
}
