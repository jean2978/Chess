package pieces;

/**
 * A record class that represents a king.
 *
 * @author Jesper
 * @version 1.0
 */
public record King(int x, int y, boolean isWhite) {
}
