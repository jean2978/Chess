package pieces;

/**
 * A record class that represents a queen.
 *
 * @author Jesper
 * @version 1.0
 */
public record Queen(int x, int y, boolean isWhite) {
}
