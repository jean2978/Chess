package pieces;

/**
 * A record class that represents a horse.
 *
 * @author Jesper
 * @version 1.0
 */
public record Horse(int x, int y, boolean isWhite) {
}
