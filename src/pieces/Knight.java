package pieces;

/**
 * A record class that represents a knight.
 *
 * @author Jesper
 * @version 1.0
 */
public record Knight( int x, int y, boolean isWhite) {
}
