package pieces;

import javax.swing.*;
import java.awt.*;
import java.io.Serial;

/**
 * This class represents a peasant piece
 * @author Jesper
 * @version 1.0
 */
public class Peasant extends JPanel {

    @Serial
    private static final long serialVersionUID = 1L;
    private final Color color;


    public Peasant(int x, int y, int width, int height, Color color) {
        super();
        this.setBounds(x, y, width, height);
        this.color = color;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(this.color);
        g.fillOval(0, 0, this.getWidth(), this.getHeight());
    }

}
