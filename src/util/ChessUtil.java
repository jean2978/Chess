package util;

import java.awt.*;

/**
 * This class contains utility methods for the chess game
 * @author Jesper
 * @version 1.0
 */
public class ChessUtil {

    //The size of a square on the board
    private static final int SQUARE_SIZE = 125;

    /**
     * This method returns the x coordinate of a square
     * @param x the x coordinate of the square
     * @return the x coordinate of the square
     */
    public static int getSquareX(int x) {
        return x * 125;
    }

    /**
     * This method returns the y coordinate of a square
     * @param y the y coordinate of the square
     * @return the y coordinate of the square
     */
    public static int getSquareY(int y) {
        return y * 125;
    }

    /**
     * This method returns color of a square
     * @param x the x coordinate of the square
     * @param y the y coordinate of the square
     * @return the color of the square
     */
    public static Color getSquareColor(int x, int y) {
        if ((x + y) % 2 == 0) {
            return Color.WHITE;
        } else {
            return Color.BLACK;
        }
    }

    /**
     * This method returns the name of a square
     * @param x the x coordinate of the square
     * @param y the y coordinate of the square
     * @return the name of the square
     */
    public String getSquareName(int x, int y) {
        String[] squareName = squareName();
        int squareNumber = (y * 8) + x;
        return squareName[squareNumber];
    }

    /**
     * This method populates an array with the names of the squares
     * @return an array with the names of the squares
     */
    public static String[] squareName() {
        String [] squareName = new String[64];
        squareName [0] = "A1";
        squareName [1] = "B1";
        squareName [2] = "C1";
        squareName [3] = "D1";
        squareName [4] = "E1";
        squareName [5] = "F1";
        squareName [6] = "G1";
        squareName [7] = "H1";
        squareName [8] = "A2";
        squareName [9] = "B2";
        squareName [10] = "C2";
        squareName [11] = "D2";
        squareName [12] = "E2";
        squareName [13] = "F2";
        squareName [14] = "G2";
        squareName [15] = "H2";
        squareName [16] = "A3";
        squareName [17] = "B3";
        squareName [18] = "C3";
        squareName [19] = "D3";
        squareName [20] = "E3";
        squareName [21] = "F3";
        squareName [22] = "G3";
        squareName [23] = "H3";
        squareName [24] = "A4";
        squareName [25] = "B4";
        squareName [26] = "C4";
        squareName [27] = "D4";
        squareName [28] = "E4";
        squareName [29] = "F4";
        squareName [30] = "G4";
        squareName [31] = "H4";
        squareName [32] = "A5";
        squareName [33] = "B5";
        squareName [34] = "C5";
        squareName [35] = "D5";
        squareName [36] = "E5";
        squareName [37] = "F5";
        squareName [38] = "G5";
        squareName [39] = "H5";
        squareName [40] = "A6";
        squareName [41] = "B6";
        squareName [42] = "C6";
        squareName [43] = "D6";
        squareName [44] = "E6";
        squareName [45] = "F6";
        squareName [46] = "G6";
        squareName [47] = "H6";
        squareName [48] = "A7";
        squareName [49] = "B7";
        squareName [50] = "C7";
        squareName [51] = "D7";
        squareName [52] = "E7";
        squareName [53] = "F7";
        squareName [54] = "G7";
        squareName [55] = "H7";
        squareName [56] = "A8";
        squareName [57] = "B8";
        squareName [58] = "C8";
        squareName [59] = "D8";
        squareName [60] = "E8";
        squareName [61] = "F8";
        squareName [62] = "G8";
        squareName [63] = "H8";
        return squareName;
    }
}
